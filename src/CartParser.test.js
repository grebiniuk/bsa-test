import CartParser from './CartParser';

let parser;
const dataWithErrors = 'Produkt name,Price,Quantity\n' +
    'Mollis consequat,9.00\n' +
    'Tvoluptatem,10.32,1\n' +
    'Scelerisque lacinia,18.90,1\n' +
    ',,\n' +
    'Condimentum aliquet,-13.90,1\n';
const expectedTestObject = {
  items:
      [
        {
          name: 'Mollis consequat',
          price: 9,
          quantity: 2,
          id: expect.stringMatching(/[\da-z-]{36}/),
        },
        {
          name: 'Tvoluptatem',
          price: 10.32,
          quantity: 1,
          id: expect.stringMatching(/[\da-z-]{36}/),
        },
        {
          name: 'Scelerisque lacinia',
          price: 18.9,
          quantity: 1,
          id: expect.stringMatching(/[\da-z-]{36}/),
        },
        {
          name: 'Consectetur adipiscing',
          price: 28.72,
          quantity: 10,
          id: expect.stringMatching(/[\da-z-]{36}/),
        },
        {
          name: 'Condimentum aliquet',
          price: 13.9,
          quantity: 1,
          id: expect.stringMatching(/[\da-z-]{36}/),
        }],
  total: 348.32,
};

beforeEach(() => {
  parser = new CartParser();
});

describe('validate(contents) - unit tests', () => {
  it('should return error when the headers name is unexpected', () => {
    const errors = parser.validate(dataWithErrors);
    expect(errors[0]).toHaveProperty('type', 'header');
  });
  it('should return error when the rows number is unexpected', () => {
    const errors = parser.validate(dataWithErrors);
    expect(errors[1]).toHaveProperty('type', 'row');
  });
  it('should return error when a cell is empty', () => {
    const errors = parser.validate(dataWithErrors);
    expect(errors[2]).toHaveProperty('type', 'cell');
  });
  it('should return error when a cell doesn\'t have positive number', () => {
    const errors = parser.validate(dataWithErrors);
    expect(errors[3]).toHaveProperty('message', 'Expected cell to be a positive number but received \"-13.90\".');
  });
  it('should return correct number of errors in the tested data', () => {
    const errors = parser.validate(dataWithErrors);
    expect(errors.length).toBe(4);
  })
});

describe('parseLine — unit tests', () => {
  it('should parse valid text line', () => {
    expect(parser.parseLine('Tvoluptatem,10.32,1')).toMatchObject(expectedTestObject.items[1]);
  });
});

describe('CartParser - integration test', () => {
  it('should read a content from csv file and return valid json in expected format', () => {
    const parsedData = parser.parse('./samples/cart.csv');
    expect(parsedData).toMatchObject(expectedTestObject);
    expect(parsedData.items).toMatchObject(expectedTestObject.items);
    expect(parsedData.items.length).toBeGreaterThan(0);
    expect(parsedData.total).toBeCloseTo(348.32);
  });
  it('should read an invalid content from csv file and throw an error', () => {
    expect(() => {
      parser.parse('./samples/cart-with-errors.csv');
    }).toThrow(/^Validation failed!$/);
  });
});